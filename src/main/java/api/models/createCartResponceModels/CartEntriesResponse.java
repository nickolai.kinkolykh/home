package api.models.createCartResponceModels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import api.models.cartEntryResponceModels.TotalPrice;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "cartType",
        "code",
        "entries",
        "guid",
        "payWithPoints",
        "pointTotalAfterOrder",
        "redeemLoyaltyPoints",
        "rewardLoyaltyPoints",
        "totalItems",
        "totalPrice",
        "totalPriceWithTax",
        "adultCustomer",
        "checkoutBlocked",
        "consistentAddress",
        "containsOtcProducts",
        "guestCheckoutAllowed",
        "homeDeliveryOnly",
        "placeOrderBlocked"
})
public class CartEntriesResponse {

    @JsonProperty("type")
    private String type;
    @JsonProperty("cartType")
    private String cartType;
    @JsonProperty("code")
    private String code;
    @JsonProperty("entries")
    private List<Object> entries = null;
    @JsonProperty("guid")
    private String guid;
    @JsonProperty("payWithPoints")
    private Integer payWithPoints;
    @JsonProperty("pointTotalAfterOrder")
    private Integer pointTotalAfterOrder;
    @JsonProperty("redeemLoyaltyPoints")
    private Integer redeemLoyaltyPoints;
    @JsonProperty("rewardLoyaltyPoints")
    private Integer rewardLoyaltyPoints;
    @JsonProperty("totalItems")
    private Integer totalItems;
    @JsonProperty("totalPrice")
    private TotalPrice totalPrice;
    @JsonProperty("totalPriceWithTax")
    private TotalPriceWithTax totalPriceWithTax;
    @JsonProperty("adultCustomer")
    private Boolean adultCustomer;
    @JsonProperty("checkoutBlocked")
    private Boolean checkoutBlocked;
    @JsonProperty("consistentAddress")
    private Boolean consistentAddress;
    @JsonProperty("containsOtcProducts")
    private Boolean containsOtcProducts;
    @JsonProperty("guestCheckoutAllowed")
    private Boolean guestCheckoutAllowed;
    @JsonProperty("homeDeliveryOnly")
    private Boolean homeDeliveryOnly;
    @JsonProperty("placeOrderBlocked")
    private Boolean placeOrderBlocked;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public CartEntriesResponse withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("cartType")
    public String getCartType() {
        return cartType;
    }

    @JsonProperty("cartType")
    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public CartEntriesResponse withCartType(String cartType) {
        this.cartType = cartType;
        return this;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public CartEntriesResponse withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("entries")
    public List<Object> getEntries() {
        return entries;
    }

    @JsonProperty("entries")
    public void setEntries(List<Object> entries) {
        this.entries = entries;
    }

    public CartEntriesResponse withEntries(List<Object> entries) {
        this.entries = entries;
        return this;
    }

    @JsonProperty("guid")
    public String getGuid() {
        return guid;
    }

    @JsonProperty("guid")
    public void setGuid(String guid) {
        this.guid = guid;
    }

    public CartEntriesResponse withGuid(String guid) {
        this.guid = guid;
        return this;
    }

    @JsonProperty("payWithPoints")
    public Integer getPayWithPoints() {
        return payWithPoints;
    }

    @JsonProperty("payWithPoints")
    public void setPayWithPoints(Integer payWithPoints) {
        this.payWithPoints = payWithPoints;
    }

    public CartEntriesResponse withPayWithPoints(Integer payWithPoints) {
        this.payWithPoints = payWithPoints;
        return this;
    }

    @JsonProperty("pointTotalAfterOrder")
    public Integer getPointTotalAfterOrder() {
        return pointTotalAfterOrder;
    }

    @JsonProperty("pointTotalAfterOrder")
    public void setPointTotalAfterOrder(Integer pointTotalAfterOrder) {
        this.pointTotalAfterOrder = pointTotalAfterOrder;
    }

    public CartEntriesResponse withPointTotalAfterOrder(Integer pointTotalAfterOrder) {
        this.pointTotalAfterOrder = pointTotalAfterOrder;
        return this;
    }

    @JsonProperty("redeemLoyaltyPoints")
    public Integer getRedeemLoyaltyPoints() {
        return redeemLoyaltyPoints;
    }

    @JsonProperty("redeemLoyaltyPoints")
    public void setRedeemLoyaltyPoints(Integer redeemLoyaltyPoints) {
        this.redeemLoyaltyPoints = redeemLoyaltyPoints;
    }

    public CartEntriesResponse withRedeemLoyaltyPoints(Integer redeemLoyaltyPoints) {
        this.redeemLoyaltyPoints = redeemLoyaltyPoints;
        return this;
    }

    @JsonProperty("rewardLoyaltyPoints")
    public Integer getRewardLoyaltyPoints() {
        return rewardLoyaltyPoints;
    }

    @JsonProperty("rewardLoyaltyPoints")
    public void setRewardLoyaltyPoints(Integer rewardLoyaltyPoints) {
        this.rewardLoyaltyPoints = rewardLoyaltyPoints;
    }

    public CartEntriesResponse withRewardLoyaltyPoints(Integer rewardLoyaltyPoints) {
        this.rewardLoyaltyPoints = rewardLoyaltyPoints;
        return this;
    }

    @JsonProperty("totalItems")
    public Integer getTotalItems() {
        return totalItems;
    }

    @JsonProperty("totalItems")
    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public CartEntriesResponse withTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
        return this;
    }

    @JsonProperty("totalPrice")
    public TotalPrice getTotalPrice() {
        return totalPrice;
    }

    @JsonProperty("totalPrice")
    public void setTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
    }

    public CartEntriesResponse withTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    @JsonProperty("totalPriceWithTax")
    public TotalPriceWithTax getTotalPriceWithTax() {
        return totalPriceWithTax;
    }

    @JsonProperty("totalPriceWithTax")
    public void setTotalPriceWithTax(TotalPriceWithTax totalPriceWithTax) {
        this.totalPriceWithTax = totalPriceWithTax;
    }

    public CartEntriesResponse withTotalPriceWithTax(TotalPriceWithTax totalPriceWithTax) {
        this.totalPriceWithTax = totalPriceWithTax;
        return this;
    }

    @JsonProperty("adultCustomer")
    public Boolean getAdultCustomer() {
        return adultCustomer;
    }

    @JsonProperty("adultCustomer")
    public void setAdultCustomer(Boolean adultCustomer) {
        this.adultCustomer = adultCustomer;
    }

    public CartEntriesResponse withAdultCustomer(Boolean adultCustomer) {
        this.adultCustomer = adultCustomer;
        return this;
    }

    @JsonProperty("checkoutBlocked")
    public Boolean getCheckoutBlocked() {
        return checkoutBlocked;
    }

    @JsonProperty("checkoutBlocked")
    public void setCheckoutBlocked(Boolean checkoutBlocked) {
        this.checkoutBlocked = checkoutBlocked;
    }

    public CartEntriesResponse withCheckoutBlocked(Boolean checkoutBlocked) {
        this.checkoutBlocked = checkoutBlocked;
        return this;
    }

    @JsonProperty("consistentAddress")
    public Boolean getConsistentAddress() {
        return consistentAddress;
    }

    @JsonProperty("consistentAddress")
    public void setConsistentAddress(Boolean consistentAddress) {
        this.consistentAddress = consistentAddress;
    }

    public CartEntriesResponse withConsistentAddress(Boolean consistentAddress) {
        this.consistentAddress = consistentAddress;
        return this;
    }

    @JsonProperty("containsOtcProducts")
    public Boolean getContainsOtcProducts() {
        return containsOtcProducts;
    }

    @JsonProperty("containsOtcProducts")
    public void setContainsOtcProducts(Boolean containsOtcProducts) {
        this.containsOtcProducts = containsOtcProducts;
    }

    public CartEntriesResponse withContainsOtcProducts(Boolean containsOtcProducts) {
        this.containsOtcProducts = containsOtcProducts;
        return this;
    }

    @JsonProperty("guestCheckoutAllowed")
    public Boolean getGuestCheckoutAllowed() {
        return guestCheckoutAllowed;
    }

    @JsonProperty("guestCheckoutAllowed")
    public void setGuestCheckoutAllowed(Boolean guestCheckoutAllowed) {
        this.guestCheckoutAllowed = guestCheckoutAllowed;
    }

    public CartEntriesResponse withGuestCheckoutAllowed(Boolean guestCheckoutAllowed) {
        this.guestCheckoutAllowed = guestCheckoutAllowed;
        return this;
    }

    @JsonProperty("homeDeliveryOnly")
    public Boolean getHomeDeliveryOnly() {
        return homeDeliveryOnly;
    }

    @JsonProperty("homeDeliveryOnly")
    public void setHomeDeliveryOnly(Boolean homeDeliveryOnly) {
        this.homeDeliveryOnly = homeDeliveryOnly;
    }

    public CartEntriesResponse withHomeDeliveryOnly(Boolean homeDeliveryOnly) {
        this.homeDeliveryOnly = homeDeliveryOnly;
        return this;
    }

    @JsonProperty("placeOrderBlocked")
    public Boolean getPlaceOrderBlocked() {
        return placeOrderBlocked;
    }

    @JsonProperty("placeOrderBlocked")
    public void setPlaceOrderBlocked(Boolean placeOrderBlocked) {
        this.placeOrderBlocked = placeOrderBlocked;
    }

    public CartEntriesResponse withPlaceOrderBlocked(Boolean placeOrderBlocked) {
        this.placeOrderBlocked = placeOrderBlocked;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartEntriesResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartEntriesResponse that = (CartEntriesResponse) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(cartType, that.cartType) &&
                Objects.equals(code, that.code) &&
                Objects.equals(entries, that.entries) &&
                Objects.equals(guid, that.guid) &&
                Objects.equals(payWithPoints, that.payWithPoints) &&
                Objects.equals(pointTotalAfterOrder, that.pointTotalAfterOrder) &&
                Objects.equals(redeemLoyaltyPoints, that.redeemLoyaltyPoints) &&
                Objects.equals(rewardLoyaltyPoints, that.rewardLoyaltyPoints) &&
                Objects.equals(totalItems, that.totalItems) &&
                Objects.equals(totalPrice, that.totalPrice) &&
                Objects.equals(totalPriceWithTax, that.totalPriceWithTax) &&
                Objects.equals(adultCustomer, that.adultCustomer) &&
                Objects.equals(checkoutBlocked, that.checkoutBlocked) &&
                Objects.equals(consistentAddress, that.consistentAddress) &&
                Objects.equals(containsOtcProducts, that.containsOtcProducts) &&
                Objects.equals(guestCheckoutAllowed, that.guestCheckoutAllowed) &&
                Objects.equals(homeDeliveryOnly, that.homeDeliveryOnly) &&
                Objects.equals(placeOrderBlocked, that.placeOrderBlocked) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                cartType,
                code,
                entries,
                guid,
                payWithPoints,
                pointTotalAfterOrder,
                redeemLoyaltyPoints,
                rewardLoyaltyPoints,
                totalItems,
                totalPrice,
                totalPriceWithTax,
                adultCustomer,
                checkoutBlocked,
                consistentAddress,
                containsOtcProducts,
                guestCheckoutAllowed,
                homeDeliveryOnly,
                placeOrderBlocked,
                additionalProperties);
    }
}
