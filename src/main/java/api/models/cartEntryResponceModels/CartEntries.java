package api.models.cartEntryResponceModels;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "entry",
        "quantity",
        "quantityAdded",
        "statusCode"
})
public class CartEntries {

    @JsonProperty("entry")
    private PrEntry entry;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("quantityAdded")
    private Integer quantityAdded;
    @JsonProperty("statusCode")
    private String statusCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("entry")
    public PrEntry getEntry() {
        return entry;
    }

    @JsonProperty("entry")
    public void setEntry(PrEntry entry) {
        this.entry = entry;
    }

    public CartEntries withEntry(PrEntry entry) {
        this.entry = entry;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CartEntries withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("quantityAdded")
    public Integer getQuantityAdded() {
        return quantityAdded;
    }

    @JsonProperty("quantityAdded")
    public void setQuantityAdded(Integer quantityAdded) {
        this.quantityAdded = quantityAdded;
    }

    public CartEntries withQuantityAdded(Integer quantityAdded) {
        this.quantityAdded = quantityAdded;
        return this;
    }

    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public CartEntries withStatusCode(String statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartEntries withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartEntries that = (CartEntries) o;
        return Objects.equals(entry, that.entry) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(quantityAdded, that.quantityAdded) &&
                Objects.equals(statusCode, that.statusCode) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entry, quantity, quantityAdded, statusCode, additionalProperties);
    }
}
