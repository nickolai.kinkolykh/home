package api.models.cartEntryResponceModels;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "availableForPickup",
        "baseOptions",
        "baseProduct",
        "categories",
        "code",
        "configurable",
        "homeDeliveryOnly",
        "isLoyaltyProduct",
        "maxOrderQuantity",
        "name",
        "pharmacy",
        "price",
        "productLoyaltyPoints",
        "purchasable",
        "stock",
        "url"
})
public class Product {

    @JsonProperty("availableForPickup")
    private Boolean availableForPickup;
    @JsonProperty("baseOptions")
    private List<BaseOption> baseOptions = null;
    @JsonProperty("baseProduct")
    private String baseProduct;
    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonProperty("code")
    private String code;
    @JsonProperty("configurable")
    private Boolean configurable;
    @JsonProperty("homeDeliveryOnly")
    private Boolean homeDeliveryOnly;
    @JsonProperty("isLoyaltyProduct")
    private Boolean isLoyaltyProduct;
    @JsonProperty("maxOrderQuantity")
    private Integer maxOrderQuantity;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pharmacy")
    private Boolean pharmacy;
    @JsonProperty("price")
    private Price price;
    @JsonProperty("productLoyaltyPoints")
    private Integer productLoyaltyPoints;
    @JsonProperty("purchasable")
    private Boolean purchasable;
    @JsonProperty("stock")
    private Stock_ stock;
    @JsonProperty("url")
    private String url;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("availableForPickup")
    public Boolean getAvailableForPickup() {
        return availableForPickup;
    }

    @JsonProperty("availableForPickup")
    public void setAvailableForPickup(Boolean availableForPickup) {
        this.availableForPickup = availableForPickup;
    }

    public Product withAvailableForPickup(Boolean availableForPickup) {
        this.availableForPickup = availableForPickup;
        return this;
    }

    @JsonProperty("baseOptions")
    public List<BaseOption> getBaseOptions() {
        return baseOptions;
    }

    @JsonProperty("baseOptions")
    public void setBaseOptions(List<BaseOption> baseOptions) {
        this.baseOptions = baseOptions;
    }

    public Product withBaseOptions(List<BaseOption> baseOptions) {
        this.baseOptions = baseOptions;
        return this;
    }

    @JsonProperty("baseProduct")
    public String getBaseProduct() {
        return baseProduct;
    }

    @JsonProperty("baseProduct")
    public void setBaseProduct(String baseProduct) {
        this.baseProduct = baseProduct;
    }

    public Product withBaseProduct(String baseProduct) {
        this.baseProduct = baseProduct;
        return this;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Product withCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public Product withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("configurable")
    public Boolean getConfigurable() {
        return configurable;
    }

    @JsonProperty("configurable")
    public void setConfigurable(Boolean configurable) {
        this.configurable = configurable;
    }

    public Product withConfigurable(Boolean configurable) {
        this.configurable = configurable;
        return this;
    }

    @JsonProperty("homeDeliveryOnly")
    public Boolean getHomeDeliveryOnly() {
        return homeDeliveryOnly;
    }

    @JsonProperty("homeDeliveryOnly")
    public void setHomeDeliveryOnly(Boolean homeDeliveryOnly) {
        this.homeDeliveryOnly = homeDeliveryOnly;
    }

    public Product withHomeDeliveryOnly(Boolean homeDeliveryOnly) {
        this.homeDeliveryOnly = homeDeliveryOnly;
        return this;
    }

    @JsonProperty("isLoyaltyProduct")
    public Boolean getIsLoyaltyProduct() {
        return isLoyaltyProduct;
    }

    @JsonProperty("isLoyaltyProduct")
    public void setIsLoyaltyProduct(Boolean isLoyaltyProduct) {
        this.isLoyaltyProduct = isLoyaltyProduct;
    }

    public Product withIsLoyaltyProduct(Boolean isLoyaltyProduct) {
        this.isLoyaltyProduct = isLoyaltyProduct;
        return this;
    }

    @JsonProperty("maxOrderQuantity")
    public Integer getMaxOrderQuantity() {
        return maxOrderQuantity;
    }

    @JsonProperty("maxOrderQuantity")
    public void setMaxOrderQuantity(Integer maxOrderQuantity) {
        this.maxOrderQuantity = maxOrderQuantity;
    }

    public Product withMaxOrderQuantity(Integer maxOrderQuantity) {
        this.maxOrderQuantity = maxOrderQuantity;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Product withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("pharmacy")
    public Boolean getPharmacy() {
        return pharmacy;
    }

    @JsonProperty("pharmacy")
    public void setPharmacy(Boolean pharmacy) {
        this.pharmacy = pharmacy;
    }

    public Product withPharmacy(Boolean pharmacy) {
        this.pharmacy = pharmacy;
        return this;
    }

    @JsonProperty("price")
    public Price getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Price price) {
        this.price = price;
    }

    public Product withPrice(Price price) {
        this.price = price;
        return this;
    }

    @JsonProperty("productLoyaltyPoints")
    public Integer getProductLoyaltyPoints() {
        return productLoyaltyPoints;
    }

    @JsonProperty("productLoyaltyPoints")
    public void setProductLoyaltyPoints(Integer productLoyaltyPoints) {
        this.productLoyaltyPoints = productLoyaltyPoints;
    }

    public Product withProductLoyaltyPoints(Integer productLoyaltyPoints) {
        this.productLoyaltyPoints = productLoyaltyPoints;
        return this;
    }

    @JsonProperty("purchasable")
    public Boolean getPurchasable() {
        return purchasable;
    }

    @JsonProperty("purchasable")
    public void setPurchasable(Boolean purchasable) {
        this.purchasable = purchasable;
    }

    public Product withPurchasable(Boolean purchasable) {
        this.purchasable = purchasable;
        return this;
    }

    @JsonProperty("stock")
    public Stock_ getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(Stock_ stock) {
        this.stock = stock;
    }

    public Product withStock(Stock_ stock) {
        this.stock = stock;
        return this;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public Product withUrl(String url) {
        this.url = url;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Product withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(availableForPickup, product.availableForPickup) && Objects.equals(baseOptions, product.baseOptions) && Objects.equals(baseProduct, product.baseProduct) && Objects.equals(categories, product.categories) && Objects.equals(code, product.code) && Objects.equals(configurable, product.configurable) && Objects.equals(homeDeliveryOnly, product.homeDeliveryOnly) && Objects.equals(isLoyaltyProduct, product.isLoyaltyProduct) && Objects.equals(maxOrderQuantity, product.maxOrderQuantity) && Objects.equals(name, product.name) && Objects.equals(pharmacy, product.pharmacy) && Objects.equals(price, product.price) && Objects.equals(productLoyaltyPoints, product.productLoyaltyPoints) && Objects.equals(purchasable, product.purchasable) && Objects.equals(stock, product.stock) && Objects.equals(url, product.url) && Objects.equals(additionalProperties, product.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(availableForPickup, baseOptions, baseProduct, categories, code, configurable, homeDeliveryOnly, isLoyaltyProduct, maxOrderQuantity, name, pharmacy, price, productLoyaltyPoints, purchasable, stock, url, additionalProperties);
    }
}