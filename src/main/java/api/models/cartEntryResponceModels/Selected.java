package api.models.cartEntryResponceModels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "priceData",
        "stock",
        "url",
        "variantOptionQualifiers"
})
public class Selected {

    @JsonProperty("code")
    private String code;
    @JsonProperty("priceData")
    private PriceData priceData;
    @JsonProperty("stock")
    private Stock stock;
    @JsonProperty("url")
    private String url;
    @JsonProperty("variantOptionQualifiers")
    private List<Object> variantOptionQualifiers = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public Selected withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("priceData")
    public PriceData getPriceData() {
        return priceData;
    }

    @JsonProperty("priceData")
    public void setPriceData(PriceData priceData) {
        this.priceData = priceData;
    }

    public Selected withPriceData(PriceData priceData) {
        this.priceData = priceData;
        return this;
    }

    @JsonProperty("stock")
    public Stock getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Selected withStock(Stock stock) {
        this.stock = stock;
        return this;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public Selected withUrl(String url) {
        this.url = url;
        return this;
    }

    @JsonProperty("variantOptionQualifiers")
    public List<Object> getVariantOptionQualifiers() {
        return variantOptionQualifiers;
    }

    @JsonProperty("variantOptionQualifiers")
    public void setVariantOptionQualifiers(List<Object> variantOptionQualifiers) {
        this.variantOptionQualifiers = variantOptionQualifiers;
    }

    public Selected withVariantOptionQualifiers(List<Object> variantOptionQualifiers) {
        this.variantOptionQualifiers = variantOptionQualifiers;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Selected withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Selected selected = (Selected) o;
        return Objects.equals(code, selected.code) && Objects.equals(priceData, selected.priceData) && Objects.equals(stock, selected.stock) && Objects.equals(url, selected.url) && Objects.equals(variantOptionQualifiers, selected.variantOptionQualifiers) && Objects.equals(additionalProperties, selected.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, priceData, stock, url, variantOptionQualifiers, additionalProperties);
    }
}