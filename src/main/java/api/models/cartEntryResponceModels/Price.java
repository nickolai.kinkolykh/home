package api.models.cartEntryResponceModels;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "currencyIso",
        "formattedOldValue",
        "formattedValue",
        "oldValue",
        "oldValueLabel",
        "priceType",
        "value"
})
public class Price {

    @JsonProperty("currencyIso")
    private String currencyIso;
    @JsonProperty("formattedOldValue")
    private String formattedOldValue;
    @JsonProperty("formattedValue")
    private String formattedValue;
    @JsonProperty("oldValue")
    private Double oldValue;
    @JsonProperty("oldValueLabel")
    private String oldValueLabel;
    @JsonProperty("priceType")
    private String priceType;
    @JsonProperty("value")
    private Double value;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("currencyIso")
    public String getCurrencyIso() {
        return currencyIso;
    }

    @JsonProperty("currencyIso")
    public void setCurrencyIso(String currencyIso) {
        this.currencyIso = currencyIso;
    }

    public Price withCurrencyIso(String currencyIso) {
        this.currencyIso = currencyIso;
        return this;
    }

    @JsonProperty("formattedOldValue")
    public String getFormattedOldValue() {
        return formattedOldValue;
    }

    @JsonProperty("formattedOldValue")
    public void setFormattedOldValue(String formattedOldValue) {
        this.formattedOldValue = formattedOldValue;
    }

    public Price withFormattedOldValue(String formattedOldValue) {
        this.formattedOldValue = formattedOldValue;
        return this;
    }

    @JsonProperty("formattedValue")
    public String getFormattedValue() {
        return formattedValue;
    }

    @JsonProperty("formattedValue")
    public void setFormattedValue(String formattedValue) {
        this.formattedValue = formattedValue;
    }

    public Price withFormattedValue(String formattedValue) {
        this.formattedValue = formattedValue;
        return this;
    }

    @JsonProperty("oldValue")
    public Double getOldValue() {
        return oldValue;
    }

    @JsonProperty("oldValue")
    public void setOldValue(Double oldValue) {
        this.oldValue = oldValue;
    }

    public Price withOldValue(Double oldValue) {
        this.oldValue = oldValue;
        return this;
    }

    @JsonProperty("oldValueLabel")
    public String getOldValueLabel() {
        return oldValueLabel;
    }

    @JsonProperty("oldValueLabel")
    public void setOldValueLabel(String oldValueLabel) {
        this.oldValueLabel = oldValueLabel;
    }

    public Price withOldValueLabel(String oldValueLabel) {
        this.oldValueLabel = oldValueLabel;
        return this;
    }

    @JsonProperty("priceType")
    public String getPriceType() {
        return priceType;
    }

    @JsonProperty("priceType")
    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public Price withPriceType(String priceType) {
        this.priceType = priceType;
        return this;
    }

    @JsonProperty("value")
    public Double getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Double value) {
        this.value = value;
    }

    public Price withValue(Double value) {
        this.value = value;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Price withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(currencyIso, price.currencyIso) && Objects.equals(formattedOldValue, price.formattedOldValue) && Objects.equals(formattedValue, price.formattedValue) && Objects.equals(oldValue, price.oldValue) && Objects.equals(oldValueLabel, price.oldValueLabel) && Objects.equals(priceType, price.priceType) && Objects.equals(value, price.value) && Objects.equals(additionalProperties, price.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyIso, formattedOldValue, formattedValue, oldValue, oldValueLabel, priceType, value, additionalProperties);
    }
}
