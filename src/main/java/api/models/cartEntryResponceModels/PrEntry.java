package api.models.cartEntryResponceModels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "configurationInfos",
        "displayOrder",
        "PrEntryNumber",
        "product",
        "quantity",
        "redeemLoyaltyPoints",
        "redeemLoyaltyPointsForProduct",
        "rewardLoyaltyPoints",
        "totalPrice"
})
public class PrEntry {

    @JsonProperty("configurationInfos")
    private List<Object> configurationInfos = null;
    @JsonProperty("displayOrder")
    private Integer displayOrder;
    @JsonProperty("EntryNumber")
    private Integer EntryNumber;
    @JsonProperty("product")
    private Product product;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("redeemLoyaltyPoints")
    private Integer redeemLoyaltyPoints;
    @JsonProperty("redeemLoyaltyPointsForProduct")
    private Integer redeemLoyaltyPointsForProduct;
    @JsonProperty("rewardLoyaltyPoints")
    private Integer rewardLoyaltyPoints;
    @JsonProperty("totalPrice")
    private TotalPrice totalPrice;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("configurationInfos")
    public List<Object> getConfigurationInfos() {
        return configurationInfos;
    }

    @JsonProperty("configurationInfos")
    public void setConfigurationInfos(List<Object> configurationInfos) {
        this.configurationInfos = configurationInfos;
    }

    public PrEntry withConfigurationInfos(List<Object> configurationInfos) {
        this.configurationInfos = configurationInfos;
        return this;
    }

    @JsonProperty("displayOrder")
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    @JsonProperty("displayOrder")
    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public PrEntry withDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
        return this;
    }

    @JsonProperty("EntryNumber")
    public Integer getPrEntryNumber() {
        return EntryNumber;
    }

    @JsonProperty("EntryNumber")
    public void setPrEntryNumber(Integer EntryNumber) {
        this.EntryNumber = EntryNumber;
    }

    public PrEntry withPrEntryNumber(Integer PrEntryNumber) {
        this.EntryNumber = PrEntryNumber;
        return this;
    }

    @JsonProperty("product")
    public Product getProduct() {
        return product;
    }

    @JsonProperty("product")
    public void setProduct(Product product) {
        this.product = product;
    }

    public PrEntry withProduct(Product product) {
        this.product = product;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public PrEntry withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("redeemLoyaltyPoints")
    public Integer getRedeemLoyaltyPoints() {
        return redeemLoyaltyPoints;
    }

    @JsonProperty("redeemLoyaltyPoints")
    public void setRedeemLoyaltyPoints(Integer redeemLoyaltyPoints) {
        this.redeemLoyaltyPoints = redeemLoyaltyPoints;
    }

    public PrEntry withRedeemLoyaltyPoints(Integer redeemLoyaltyPoints) {
        this.redeemLoyaltyPoints = redeemLoyaltyPoints;
        return this;
    }

    @JsonProperty("redeemLoyaltyPointsForProduct")
    public Integer getRedeemLoyaltyPointsForProduct() {
        return redeemLoyaltyPointsForProduct;
    }

    @JsonProperty("redeemLoyaltyPointsForProduct")
    public void setRedeemLoyaltyPointsForProduct(Integer redeemLoyaltyPointsForProduct) {
        this.redeemLoyaltyPointsForProduct = redeemLoyaltyPointsForProduct;
    }

    public PrEntry withRedeemLoyaltyPointsForProduct(Integer redeemLoyaltyPointsForProduct) {
        this.redeemLoyaltyPointsForProduct = redeemLoyaltyPointsForProduct;
        return this;
    }

    @JsonProperty("rewardLoyaltyPoints")
    public Integer getRewardLoyaltyPoints() {
        return rewardLoyaltyPoints;
    }

    @JsonProperty("rewardLoyaltyPoints")
    public void setRewardLoyaltyPoints(Integer rewardLoyaltyPoints) {
        this.rewardLoyaltyPoints = rewardLoyaltyPoints;
    }

    public PrEntry withRewardLoyaltyPoints(Integer rewardLoyaltyPoints) {
        this.rewardLoyaltyPoints = rewardLoyaltyPoints;
        return this;
    }

    @JsonProperty("totalPrice")
    public TotalPrice getTotalPrice() {
        return totalPrice;
    }

    @JsonProperty("totalPrice")
    public void setTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
    }

    public PrEntry withTotalPrice(TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PrEntry withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrEntry prEntry = (PrEntry) o;
        return Objects.equals(configurationInfos, prEntry.configurationInfos) && Objects.equals(displayOrder, prEntry.displayOrder) && Objects.equals(EntryNumber, prEntry.EntryNumber) && Objects.equals(product, prEntry.product) && Objects.equals(quantity, prEntry.quantity) && Objects.equals(redeemLoyaltyPoints, prEntry.redeemLoyaltyPoints) && Objects.equals(redeemLoyaltyPointsForProduct, prEntry.redeemLoyaltyPointsForProduct) && Objects.equals(rewardLoyaltyPoints, prEntry.rewardLoyaltyPoints) && Objects.equals(totalPrice, prEntry.totalPrice) && Objects.equals(additionalProperties, prEntry.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configurationInfos, displayOrder, EntryNumber, product, quantity, redeemLoyaltyPoints, redeemLoyaltyPointsForProduct, rewardLoyaltyPoints, totalPrice, additionalProperties);
    }
}