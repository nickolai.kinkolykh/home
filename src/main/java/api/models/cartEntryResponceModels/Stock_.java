package api.models.cartEntryResponceModels;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "stockLevel",
        "stockLevelStatus"
})
public class Stock_ {

    @JsonProperty("stockLevel")
    private Integer stockLevel;
    @JsonProperty("stockLevelStatus")
    private String stockLevelStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("stockLevel")
    public Integer getStockLevel() {
        return stockLevel;
    }

    @JsonProperty("stockLevel")
    public void setStockLevel(Integer stockLevel) {
        this.stockLevel = stockLevel;
    }

    public Stock_ withStockLevel(Integer stockLevel) {
        this.stockLevel = stockLevel;
        return this;
    }

    @JsonProperty("stockLevelStatus")
    public String getStockLevelStatus() {
        return stockLevelStatus;
    }

    @JsonProperty("stockLevelStatus")
    public void setStockLevelStatus(String stockLevelStatus) {
        this.stockLevelStatus = stockLevelStatus;
    }

    public Stock_ withStockLevelStatus(String stockLevelStatus) {
        this.stockLevelStatus = stockLevelStatus;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Stock_ withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock_ stock_ = (Stock_) o;
        return Objects.equals(stockLevel, stock_.stockLevel) && Objects.equals(stockLevelStatus, stock_.stockLevelStatus) && Objects.equals(additionalProperties, stock_.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockLevel, stockLevelStatus, additionalProperties);
    }
}
