package api.models.cartEntryResponceModels;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "stockLevel"
})
public class Stock {

    @JsonProperty("stockLevel")
    private Integer stockLevel;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("stockLevel")
    public Integer getStockLevel() {
        return stockLevel;
    }

    @JsonProperty("stockLevel")
    public void setStockLevel(Integer stockLevel) {
        this.stockLevel = stockLevel;
    }

    public Stock withStockLevel(Integer stockLevel) {
        this.stockLevel = stockLevel;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Stock withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return Objects.equals(stockLevel, stock.stockLevel) && Objects.equals(additionalProperties, stock.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockLevel, additionalProperties);
    }
}
