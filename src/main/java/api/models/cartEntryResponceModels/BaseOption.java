package api.models.cartEntryResponceModels;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "selected",
        "variantType"
})
public class BaseOption {

    @JsonProperty("selected")
    private Selected selected;
    @JsonProperty("variantType")
    private String variantType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("selected")
    public Selected getSelected() {
        return selected;
    }

    @JsonProperty("selected")
    public void setSelected(Selected selected) {
        this.selected = selected;
    }

    public BaseOption withSelected(Selected selected) {
        this.selected = selected;
        return this;
    }

    @JsonProperty("variantType")
    public String getVariantType() {
        return variantType;
    }

    @JsonProperty("variantType")
    public void setVariantType(String variantType) {
        this.variantType = variantType;
    }

    public BaseOption withVariantType(String variantType) {
        this.variantType = variantType;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public BaseOption withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseOption that = (BaseOption) o;
        return Objects.equals(selected, that.selected) &&
                Objects.equals(variantType, that.variantType) &&
                Objects.equals(additionalProperties, that.additionalProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(selected, variantType, additionalProperties);
    }
}