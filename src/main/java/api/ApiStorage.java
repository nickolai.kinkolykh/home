package api;

import org.apache.commons.lang3.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

public class ApiStorage {

    private static HashMap<String, Object> values = new HashMap<>();
    private static ApiStorage apiStorage = null;

    public static Map<String, Object> getValues() {
        return values;
    }

    public static Object getValueByKey(String key) {
        if (values.containsKey(key)) {
            return values.get(key);
        }
        return null;
    }

    public static void setValues(String key, Object value) {
        if (apiStorage == null) {
            apiStorage = new ApiStorage();
        }
        values.put(key, value);
    }

    private ApiStorage() {
    }
}
