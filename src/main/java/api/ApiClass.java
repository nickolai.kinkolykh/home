package api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;

import static java.lang.String.format;

public class ApiClass {

    private static final String URL = "https://www.kruidvat.nl/";
    private static final String CREATE_CART_ENDPOINT = "/api/v2/kvn/users/anonymous/carts";
    private static final String ADD_PRODUCT_TO_CART = "/api/v2/kvn/users/anonymous/carts/%s/entries";

    public static RequestSpecification setSpec() {
        return new RequestSpecBuilder().setBaseUri(URL)
                .addFilter(new ResponseLoggingFilter()).addFilter(new RequestLoggingFilter()).build();
    }

    public static String CREATE_CART_ENDPOINT() {
        return CREATE_CART_ENDPOINT;
    }

    public static String ADD_TO_CART_ENDPOINT() {
        if (ApiStorage.getValueByKey("guid") != null) {
            return format(ADD_PRODUCT_TO_CART, ApiStorage.getValueByKey("guid"));
        } else
            return null;
    }

    public static String GET_GUID() {
        if (ApiStorage.getValueByKey("guid") != null) {
            return (String) ApiStorage.getValueByKey("guid");
        } else
            return null;
    }
}
