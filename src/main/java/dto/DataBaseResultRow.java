package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class DataBaseResultRow {

    public String userName;
    public String userEmail;
    public String userPassword;
    public String salary;

    public DataBaseResultRow() {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }

    public DataBaseResultRow(String userName, String userEmail, String userPassword) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }

    public DataBaseResultRow(String userName, String userEmail, String userPassword, String salary) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.salary = salary;
    }
}
