package desktop.fragments.kruidvat;

import abstractClasses.fragment.AbstractFragment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeliveryOrderInformation extends AbstractFragment {

    @FindBy(css = ".checkout-delivery-and-order-information.checkout-delivery-and-order-information--cart")
    private WebElement fragmentRootElement;

    public DeliveryOrderInformation() {
        setRootElement(fragmentRootElement);
    }

    private static final By TOTAL_AMOUNT = By.cssSelector(".cart-summary .cart-summary__total-price ");

    public String getTotalAmount() {
        return getElementText(TOTAL_AMOUNT);
    }
}
