package desktop.fragments.kruidvat;

import abstractClasses.fragment.AbstractFragment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BannerFragment extends AbstractFragment {

    @FindBy(css = "#onetrust-banner-sdk")
    private WebElement fragmentRootElement;

    private static final By ACCEPT_BANNER_BUTTON = By.cssSelector("#onetrust-accept-btn-handler");
    private static final By SELECT_NL_LANGUAGE = By.xpath(".//e2-action-button[@button-label='Naar Kruidvat.nl']");

    public BannerFragment() {
        setRootElement(fragmentRootElement);
    }

    public BannerFragment clickOnAcceptButton() {
        clickElement(ACCEPT_BANNER_BUTTON);
        return this;
    }

    public BannerFragment clickOnNlButton() {
        isChildElementVisible(SELECT_NL_LANGUAGE);
        clickElement(SELECT_NL_LANGUAGE);
        return this;
    }
}
