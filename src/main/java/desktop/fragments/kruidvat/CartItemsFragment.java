package desktop.fragments.kruidvat;

import abstractClasses.fragment.AbstractFragment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class CartItemsFragment extends AbstractFragment {

    @FindBy(css = "e2-product-summary")
    private WebElement fragmentRootElement;

    public CartItemsFragment() {
        setRootElement(fragmentRootElement);
    }

    private static final By PRODUCT_NAME = By.cssSelector(".product-summary__description-name");
    private static final By PRODUCT_PRICE = By.cssSelector(".product-summary__nowrap .formated-price");
    private static final By PRODUCT_TOTAL_PRICE = By.cssSelector(".product-summary__price-total .formated-price");

    public String getProductName() {
        return getElementText(PRODUCT_NAME);
    }

    public Double getProductPrice() {
        return Double.parseDouble(getElementText(PRODUCT_PRICE).substring(2).replace(",", "."));
    }

    public Double getTotalPrice() {
        return Double.parseDouble(getElementText(PRODUCT_TOTAL_PRICE).substring(2).replace(",", "."));
    }
}
