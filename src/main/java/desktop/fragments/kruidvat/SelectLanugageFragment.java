package desktop.fragments.kruidvat;

import abstractClasses.fragment.AbstractFragment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SelectLanugageFragment extends AbstractFragment {

    @FindBy(css = ".multilanguage-popup")
    private WebElement fragmentRootElement;

    public SelectLanugageFragment() {
        setRootElement(fragmentRootElement);
    }

    private static final By SELECT_NL_LANGUAGE = By.xpath(".//e2-action-button[@button-label='Naar Kruidvat.nl']");

    public SelectLanugageFragment clickOnNlButton() {
        isChildElementVisible(SELECT_NL_LANGUAGE);
        clickElement(SELECT_NL_LANGUAGE);
        return this;
    }
}
