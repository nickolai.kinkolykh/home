package desktop.pages.kruidvat;

import abstractClasses.page.AbstractPage;

public class KruidvatPage extends AbstractPage {
    private static final String PAGE_URL = "https://www.kruidvat.nl/cart";

    public KruidvatPage() {
        setPageUrl(PAGE_URL);
    }
}
