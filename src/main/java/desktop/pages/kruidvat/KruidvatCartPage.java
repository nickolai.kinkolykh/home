package desktop.pages.kruidvat;

import abstractClasses.page.AbstractPage;

public class KruidvatCartPage extends AbstractPage {
    private static final String PAGE_URL = "https://www.kruidvat.nl/";

    public KruidvatCartPage() {
        setPageUrl(PAGE_URL);
    }
}
