package desktop.pages.bookdepository;

import abstractClasses.page.AbstractPage;

public class HomePage extends AbstractPage {

    private static final String PAGE_URL = "https://www.bookdepository.com/";

    public HomePage() {
        setPageUrl(PAGE_URL);
    }
}
