package desktop.pages.bookdepository;

import abstractClasses.page.AbstractPage;

public class SearchPage extends AbstractPage {
    private static final String PAGE_URL = "https://www.bookdepository.com/search?searchTerm=";

    public SearchPage() {
        setPageUrl(PAGE_URL);
    }
}
