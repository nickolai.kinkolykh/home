package desktop.pages.bookdepository;

import abstractClasses.page.AbstractPage;

public class CheckoutPage extends AbstractPage {
    private static final String PAGE_URL = "https://www.bookdepository.com/checkout/guest";

    public CheckoutPage() {
        setPageUrl(PAGE_URL);
    }

}
