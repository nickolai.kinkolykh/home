package desktop.pages.bookdepository;

import abstractClasses.page.AbstractPage;

public class BasketPage extends AbstractPage {
    private static final String PAGE_URL = "https://www.bookdepository.com/basket";



    public BasketPage() {
        setPageUrl(PAGE_URL);
    }
}
