package desktop.pages;

import abstractClasses.page.AbstractPage;
import desktop.pages.bookdepository.BasketPage;
import desktop.pages.bookdepository.CheckoutPage;
import desktop.pages.bookdepository.HomePage;
import desktop.pages.bookdepository.SearchPage;
import desktop.pages.kruidvat.KruidvatCartPage;
import desktop.pages.kruidvat.KruidvatPage;
import driver.SingletonDriver;

import java.util.function.Supplier;

import static java.util.Arrays.stream;

public enum Navigator {

    HOME_PAGE("Initial home page", HomePage::new), SEARCH_PAGE("Search page", SearchPage::new),
    BASKET_PAGE("Basket page", BasketPage::new), CHECKOUT("Checkout page", CheckoutPage::new),
    KRUIDVAT_PAGE("Kruidvat home page", KruidvatPage::new),
    KRUIDVAT_CART_PAGE("Kruidvat cart page", KruidvatCartPage::new);

    private String name;
    private AbstractPage page;
    private static String PAGE_ERROR_MESSAGE = "No Page can be found by this name";

    Navigator(String pageName, Supplier<AbstractPage> pageSupplier) {
        this.name = pageName;
        this.page = pageSupplier.get();
    }

    public static AbstractPage visitPageByName(String givenPageName) {
        return getNavigatorPageByName(givenPageName).visit();
    }

    private static Navigator getNavigatorPageByName(String pageName) {
        return stream(Navigator.values())
                .filter(page -> page.name.equals(pageName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(PAGE_ERROR_MESSAGE));
    }

    public AbstractPage visit() {
        page.visit();
        return page;
    }

    private static AbstractPage getPage(String pageName) {
        return getNavigatorPageByName(pageName).page;
    }

    public static String getPageURLByName(String pageName) {
        return getPage(pageName).getPageUrl();
    }

    public static void visitDirectSite(String site) {
        SingletonDriver.getDriver().navigate().to(site);
    }
}
