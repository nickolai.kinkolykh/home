package db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class JdbcDriverSetup {

    private Connection connection;
    private static Logger logger = LoggerFactory.getLogger(JdbcDriverSetup.class);

    public void dbDriverSetup() {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            logger.error("Exception caught during creating new instance of Driver");
            e.printStackTrace();
            return;
        }

        logger.info("DB driver instance created");

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "1234");
        } catch (SQLException e) {
            logger.error("incorrect DB url, or credentials");
            e.printStackTrace();
        }

        if (connection != null) {
            logger.info("connection successfull");
        } else {
            logger.error("connection not established");
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
