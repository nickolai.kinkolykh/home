package driver;


import com.codeborne.selenide.*;
import db.JdbcDriverSetup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Browsers.FIREFOX;
import static constants.Constants.IMPLICITLY_WAIT_TIMEOUT;
import static driver.CapabilitiesHelper.getChromeOptions;
import static driver.ConfigurationService.getTestProperty;

public class SingletonDriver {

    private static Logger logger = LoggerFactory.getLogger(SingletonDriver.class);

    private static void createDriver() {

        String browser = getTestProperty("selenium.browser");

        switch (browser) {
            case "Chrome":
                WebDriverManager.chromedriver().browserVersion("89.0.4389.82").setup();
                logger.info("Chromedriver set through properties");
                WebDriverRunner.setWebDriver(new ChromeDriver(getChromeOptions()));
                WebDriverRunner.getWebDriver().manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
                break;
            case "Firefox":
                WebDriverManager.firefoxdriver().browserVersion("78.7.0esr").setup();
                logger.info("FireFox set through properties");
                WebDriverRunner.setWebDriver(new FirefoxDriver());
                WebDriverRunner.getWebDriver().manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
                break;
            default:
                throw new IllegalStateException("This driver is not supported");
        }
    }

    private static ThreadLocal <WebDriver> instance = new ThreadLocal<WebDriver>(){
        @Override
        protected WebDriver initialValue()
        {
            createDriver();
            WebDriverRunner.getWebDriver().manage().window().maximize();
            WebDriverRunner.getWebDriver().manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT, TimeUnit.SECONDS);
            return WebDriverRunner.getWebDriver();
        }
    };


    public static WebDriver getDriver() {
        return instance.get();
    }
}
