package driver;

import org.openqa.selenium.chrome.ChromeOptions;

import static driver.ConfigurationService.getTestProperty;

public class CapabilitiesHelper {

    public static ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(getTestProperty("test.type"));
        chromeOptions.addArguments(getTestProperty("ignore.cert"));
        chromeOptions.addArguments("--window-size=1920,1080");
//        chromeOptions.addArguments("--headless");
        return chromeOptions;
    }
}
