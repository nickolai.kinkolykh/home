package stepDefs.apiStepDefs;

import api.ApiClass;
import api.ApiStorage;
import api.models.cartEntryResponceModels.CartEntries;
import api.models.createCartResponceModels.CartEntriesResponse;
import desktop.fragments.kruidvat.BannerFragment;
import desktop.fragments.kruidvat.CartItemsFragment;
import desktop.fragments.kruidvat.SelectLanugageFragment;
import driver.SingletonDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.Cookie;

import java.util.HashMap;
import java.util.Map;

import static api.ApiClass.*;
import static io.restassured.RestAssured.given;
import static utils.TemplateUtils.FILE_PATH;
import static utils.TemplateUtils.getTemplateWithValues;

public class ApiStepDefs {
    Response response;
    Map<String, Object> values = new HashMap<>();
    CartEntries cartEntries = new CartEntries();
    Cookie newGuid;
    CartEntriesResponse cartEntriesResponse = new CartEntriesResponse();
    BannerFragment bannerFragment = new BannerFragment();
    SelectLanugageFragment selectLanugageFragment = new SelectLanugageFragment();
    CartItemsFragment cartItemsFragment = new CartItemsFragment();

    @When("I perform POST request to \"CreateCartEndpoint\" endpoint")
    public void iPerformPOSTRequestToEndpoint() {
        response = given().spec(setSpec()).contentType(ContentType.JSON).post(CREATE_CART_ENDPOINT());
    }

    @Then("new guid is successfully parsed from response")
    public void guidIsSuccessfullyParsed() {
        cartEntriesResponse = response.as(CartEntriesResponse.class);
        ApiStorage.setValues("guid", cartEntriesResponse.getGuid());
    }

    @And("I prepare request template with values code - {string} and quantity - {string}")
    public void iPrepareRequestTemplateWithValuesCodeAndQuantity(String code, String quantity) {
        values.put("code", code);
        values.put("quantity", quantity);
    }

    @And("I perform POST with specific template payload to \"Add product to basket\" endpoint")
    public void iPerformPOSTWithSpecificTemplatePayloadToEndpoint() {
        cartEntries = given().spec(setSpec()).contentType(ContentType.JSON)
                .body(getTemplateWithValues(FILE_PATH, values))
                .post(ADD_TO_CART_ENDPOINT()).as(CartEntries.class);
    }

    @And("old guid is successfully parsed from cookies")
    public void guidIsSuccessfullyParsedFromCookies() {
        String guidCookie = SingletonDriver.getDriver().manage().getCookieNamed("kvn-cart").getValue();
        ApiStorage.setValues("OldGuid", guidCookie);
    }

    @And("I handle popup windows")
    public void iHandlePopupWindows() {
        bannerFragment.clickOnAcceptButton();
        selectLanugageFragment.clickOnNlButton();
    }

    @Then("I validate that product displayed on cart is equal to \"Add product to basket\" POST response")
    public void iValidateThatProductDisplayedOnCartIsEqualToPOSTResponse() {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(cartItemsFragment.getProductName())
                .as("Name displayed on UI isn't expected as it in POST response")
                .isEqualTo(cartEntries.getEntry().getProduct().getName());
        softly.assertThat(cartItemsFragment.getProductPrice())
                .as("Product Price displayed on UI isn't expect to product price in POST response")
                .isEqualTo(cartEntries.getEntry().getProduct().getPrice().getValue());
        softly.assertThat(cartItemsFragment.getTotalPrice())
                .as("Total Price displayed on UI isn't expect to product price in POST response")
                .isEqualTo(cartEntries.getEntry().getTotalPrice().getValue());
        softly.assertAll();
    }

    @And("I replace giud and kvn-cart cookies")
    public void iReplaceGiudAndKvnCartCookies() {
        newGuid = new Cookie("kvn-cart", ApiClass.GET_GUID());
        SingletonDriver.getDriver().manage().deleteAllCookies();

    }

    @And("I refresh browser tab")
    public void iRefreshBrowserTab() {
        SingletonDriver.getDriver().manage().addCookie(newGuid);
        SingletonDriver.getDriver().navigate().refresh();
    }
}
