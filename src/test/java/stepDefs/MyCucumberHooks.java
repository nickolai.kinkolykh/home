package stepDefs;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.ScreenShooter;
import driver.SingletonDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Selenide.screenshot;

public class MyCucumberHooks {
    Logger logger =  LoggerFactory.getLogger(MyCucumberHooks.class);

    @Before
    public void setUp() {
        SingletonDriver.getDriver();
        logger.info("Before hook execution -> Driver started");
    }

//    ------------------------DEFAULT IMPLEMENTATION OF SCREENSHOTS WITH SELENIUM WEBDRIVER ---------------------------
//    @After
//    public void tearDown(Scenario scenario) {
//        logger.info("After hook execution -> Capturing screenshot");
//        if (scenario.isFailed()) {
//            logger.warn("Scenario: "+ scenario.getName()+"is failed, trying to capture screenshot");
//            File scrFile = ((TakesScreenshot) SingletonDriver.getDriver()).getScreenshotAs(OutputType.FILE);
//            try {
//                FileUtils.copyFile(scrFile, new File("src/test/capturedScreenshots/"+scenario.getName()+" - failed.png"));
//            } catch (IOException e) {
//                logger.error("could not save failure screenshot");
//                e.printStackTrace();
//            }
//        }
//        logger.info("After hook execution -> Screenshot captured in src/test/capturedScreenshots");
//        SingletonDriver.getDriver().close();
//    }

    @After
    public void tearDown(Scenario scenario) {
        Configuration.reportsFolder = "src/test/capturedScreenshots";
        logger.info("After hook execution -> Capturing screenshot");
        if (scenario.isFailed()) {
            logger.warn("Scenario: "+ scenario.getName()+"is failed, trying to capture screenshot");
            screenshot(scenario.getName());
            logger.info("After hook execution -> Screenshot of failed scenario captured in src/test/capturedScreenshots");
        }
        SingletonDriver.getDriver().close();
        screenshot(scenario.getName());
        logger.info("After hook execution -> Screenshot of successful scenario captured in src/test/capturedScreenshots");
    }
}
