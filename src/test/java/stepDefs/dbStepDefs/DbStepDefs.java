package stepDefs.dbStepDefs;

import db.JdbcDriverSetup;
import dto.DataBaseResultRow;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class DbStepDefs {

    private JdbcDriverSetup jdbcDriverSetup = new JdbcDriverSetup();
    private Connection connection;
    private Statement stm;
    private ResultSet resultSet;
    private DataBaseResultRow actualResult;

    @Given("I create connection to my local data base")
    public void iCreateConnectionToMyLocalDataBase() {
        jdbcDriverSetup.dbDriverSetup();
        connection = jdbcDriverSetup.getConnection();
        assertNotNull(connection);
    }

    @When("I perform {string} query to get information from DB")
    public void iPerformQueryToGetInformationFromDB(String sqlQuery) {
        try {
            stm = connection.createStatement();
            resultSet = stm.executeQuery(sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Then("I validate that users grabbed successful")
    public void iValidateThatUserGrabbedSuccessful(List<DataBaseResultRow> rows) {
        List<DataBaseResultRow> actualUserList = new ArrayList<>();
        SoftAssertions softly = new SoftAssertions();
        try {
            while (resultSet.next()) {
                actualResult = new DataBaseResultRow(
                        resultSet.getString("username"),
                        resultSet.getString("email"),
                        resultSet.getString("password"));
                actualUserList.add(actualResult);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < actualUserList.size(); i ++) {
            softly.assertThat(rows.get(i).userName).as("").isEqualTo(actualUserList.get(i).userName);
            softly.assertThat(rows.get(i).userEmail).as("").isEqualTo(actualUserList.get(i).userEmail);
            softly.assertThat(rows.get(i).userPassword).as("").isEqualTo(actualUserList.get(i).userPassword);
        }
        softly.assertAll();
    }

    @Then("I validate that users grabbed successfully with salary")
    public void iValidateThatUsersGrabbedSuccessfullyWithSalary(List<DataBaseResultRow> rows) {
        List<DataBaseResultRow> actualUserList = new ArrayList<>();
        SoftAssertions softly = new SoftAssertions();
        try {
            while (resultSet.next()) {
                actualResult = new DataBaseResultRow(
                        resultSet.getString("username"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("salary"));
                actualUserList.add(actualResult);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < actualUserList.size(); i ++) {
            softly.assertThat(rows.get(i).userName).as("").isEqualTo(actualUserList.get(i).userName);
            softly.assertThat(rows.get(i).userEmail).as("").isEqualTo(actualUserList.get(i).userEmail);
            softly.assertThat(rows.get(i).userPassword).as("").isEqualTo(actualUserList.get(i).userPassword);
            softly.assertThat(rows.get(i).salary).as("").isEqualTo(actualUserList.get(i).salary);
        }
        softly.assertAll();
    }
}
