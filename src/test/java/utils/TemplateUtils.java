package utils;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.io.FileUtils;
import org.apache.commons.text.StringSubstitutor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class TemplateUtils {

    public static final String FILE_PATH = "src/test/resources/templates/post_cart.txt";

    public static void makeModification() {
        String template = getMockedTemplate("src/test/resources/templates/post_cart.txt");
        DocumentContext context = JsonPath.parse(template);
        System.out.println(context.jsonString());
        return;
    }

    public static String getTemplateWithValues(String path, Map<String, Object> values) {
        String mockedTemplate = getMockedTemplate(path);
        StringSubstitutor stringSubstitutor = new StringSubstitutor(values, "{{", "}}");
        return stringSubstitutor.replace(mockedTemplate).replaceAll("\\s","");
    }

    private static String getMockedTemplate(String path) {
        File file = new File(path);
        String content = null;
        try {
            content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(content);
        return content;
    }

}
