import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"}
        ,glue = "stepDefs"
        ,plugin = {"pretty","com.epam.reportportal.cucumber.ScenarioReporter"}
        ,tags = "@Runme or @ApiTest"
        ,dryRun = false
        ,publish = true
)
public class RunCucumberTest {

}
