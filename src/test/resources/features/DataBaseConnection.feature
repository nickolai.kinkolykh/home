Feature: Create connection to Database

  Scenario: Connect to Local MySql Database and grab info from table
    Given I create connection to my local data base
    When I perform "select * from user;" query to get information from DB
    Then I validate that users grabbed successful
      | userName       | userEmail              | UserPassword  |
      | new user name  | my_new_email@test.com  | password1234  |
      | new user name2 | my_new_email2@test.com | password12345 |
      | new user name3 | my_new_email3@test.com | password12345 |

  Scenario: Connect to Local MySql Database and grab info from join of two tables
    Given I create connection to my local data base
    When I perform "SELECT user.username, user.email, user.password, salary.salary FROM `test`.`user` JOIN `test`.`salary` ON salary.username = user.username;" query to get information from DB
    Then I validate that users grabbed successfully with salary
      | userName       | userEmail              | UserPassword  | Salary |
      | new user name2 | my_new_email2@test.com | password12345 | 500    |
      | new user name3 | my_new_email3@test.com | password12345 | 400    |
      | new user name  | my_new_email@test.com  | password1234  | 700    |


