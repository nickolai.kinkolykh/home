@ApiTest
Feature: Populate cart through api call and validate that UI shows results

  Scenario: Populate cart through api call and validate that UI shows results
    Given I open the site "https://www.kruidvat.nl/"
    And old guid is successfully parsed from cookies
    And I perform POST request to "CreateCartEndpoint" endpoint
    And new guid is successfully parsed from response
    And I replace giud and kvn-cart cookies
    And I refresh browser tab
    And I handle popup windows
    And I prepare request template with values code - "4929800" and quantity - "1"
    And I perform POST with specific template payload to "Add product to basket" endpoint
    When I open the site "https://www.kruidvat.nl/cart"
    Then I validate that product displayed on cart is equal to "Add product to basket" POST response